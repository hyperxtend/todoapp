import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, Container, } from "react-bootstrap";
import CreateTasks from "./Components/CreateTasks";
import Tasks from "./Components/TasksToDo";

function App() {
  return (
  <Router>
    <div className="App">
        <Navbar bg="light" className="navbar">
                <Link className="navLink" to={"/create"}>Create Tasks</Link>
                <Link className="navLink" to={"/todoList"}>Task List</Link>
        </Navbar>
      <Container>
        <div className="row">
          <div className="col-md-12">
            <Switch>
              <Route exact path='/' component={CreateTasks} />
              <Route path='/create' component={CreateTasks} />
              <Route path="/todoList" component={Tasks} />
            </Switch>
          </div>
        </div>
      </Container>
    </div>
  </Router>
  );
}

export default App;