import mongoose from 'mongoose';
import express from 'express';
import router = express.Router;
import user from '../models/user-schema';
import {check, validationResult} from "express-validator"
const todoRoutes = router();

todoRoutes.get('/', (req, res, next) => {
    user.find((error, data) => {
        if (error) {
            return next(error)
        } else {
            res.json(data)
            console.log(data)
        }
    })
})

todoRoutes.post('/',(req: any, res: any, next: any) => {
    user.create(req.body, (error: any, data: string) => {
        const errors = validationResult(req)
        console.log
        if (!errors.isEmpty()) {
            return res.status(204).json(null)
        } else {
            res.json(data)
            console.log(data)
             
        }
    })
});



todoRoutes.delete('/:id',(req, res, next) => {
    const id: string = req.params.id;
    user.findOneAndDelete( {_id: id}, (error, result) => {
        if(error) {
            console.log(error)
        } else {
            res.json({message: "Task Deleted", id: id});
            // console.log("Result:", result);
    
        }
    })
    console.log("Item Deleted:", id)
    
})
export default todoRoutes;