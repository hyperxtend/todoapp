import React, { Component } from 'react';
import axios from 'axios';
import DataTable from './ToDoList';
import { Table } from "react-bootstrap"

interface MyState {
  usersCollection: [],
}
interface MyProps {
    data: object
}

export default class Tasks extends Component<MyProps, MyState>{

    constructor(props: MyProps) {
        super(props);
        this.state = { usersCollection: [] };
    }

    componentDidMount() {
        axios.get('http://localhost:4000/')
            .then(res => {
                this.setState({usersCollection: res.data});
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    dataTable() {
        return this.state.usersCollection.map((data, i) => {
            return <DataTable  obj={data}  key={i}/>;
        });
    }

    render() {
        return (
            <div className="wrapperTasks">
                <div className="container">
                    <Table className="table">
                        <thead className="tableHeader">
                            <tr>
                                <td className="tableHead">Title:</td>
                                <td className="tableHead">Description:</td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.dataTable()}
                        </tbody>
                    </Table>
                </div>
            </div>
        )
    }
}