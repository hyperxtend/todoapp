import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let userSchema = new Schema({
    title: {
        type: String,
    },
    description: {
        type: String
    }
}, {
        collection: 'users'
    })

export default mongoose.model('User', userSchema)