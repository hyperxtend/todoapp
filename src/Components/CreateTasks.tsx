import React, { Component } from 'react';
import axios from 'axios';
import {Button, Form, FormControl, InputGroup} from "react-bootstrap";

type MyProps = {
    event: Event,
    target: EventTarget,
    onSubmit: Event,
    preventDefault: Function, 
};

type MyState = {
    title: string,
    description: string
};

export default class CreateTask extends Component<MyProps, MyState> {

    constructor(props: MyProps) {
        super(props)
        this.onChangeTaskTitle = this.onChangeTaskTitle.bind(this);
        this.onChangeTaskDescription = this.onChangeTaskDescription.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            description: ''
        }
    }
    onChangeTaskTitle(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ title: event.target.value})
    }

    onChangeTaskDescription(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({ description: event.target.value})
    } 
    onSubmit(event: any) {
        // event.preventDefault()
        const TaskObject = {
            title: this.state.title,
            description: this.state.description
        };
        this.setState({title: "", description: "" })
        axios.post('http://localhost:4000/', TaskObject, {
            headers: {"Content-Type" : "application/json"}
        })
            .then((res) => {
                console.log(res.data)
            }).catch((error) => {
                console.log(error)
            });
           
        
    }
    render() {
        return (
            <div className="formWrapper">
                <Form className="taskForm" >
                    <div className="formGroup">
                        <InputGroup size="lg" className="mb-3" onChange={this.onChangeTaskTitle} >
                        <InputGroup.Prepend>
                        <InputGroup.Text >Title:</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control aria-label="Small" placeholder="Add title" required/>
                        </InputGroup>
                    </div>
                    <div className="formGroup">
                    <InputGroup className="mb-4" onChange={this.onChangeTaskDescription} defaultValue={this.state.description} >
                        <InputGroup.Prepend>
                        <InputGroup.Text >Description:</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl aria-label="Large" placeholder="Add description" required/>
                        </InputGroup>
                    </div>
                    <div className="formGroup">
                        <Button type="submit" variant="outline-secondary" onClick={this.onSubmit}>Add Task</Button>
                    </div>
                    </Form> 
            </div>
        )
    }
}