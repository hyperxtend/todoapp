import React, { Component } from 'react';
import axios from "axios";
import {Button, Table} from "react-bootstrap";


interface MyProps{
    obj: {
        _id: string,
        title: string,
        description: string
    }
}

interface Response {
    message: string
    id: string
}

class DataTable extends Component<MyProps>{
    constructor(props: MyProps) {
        super(props)
        this.removeItem = this.removeItem.bind(this);
        
    }

    removeItem (id: string, event: Event){
    event.preventDefault();
    axios.delete(`http://localhost:4000/${id}`, {
    headers: {"Content-Type" : "application/json"}}
        ).then(res => {
            console.log("Results ", res);
            
            
        })
        window.location.reload()
    }
    render() {
        return (
            <tr>
                <td>
                    {this.props.obj.title}
                </td>
                <td>
                    {this.props.obj.description}
                </td>
                <div>
                <Button className="itemRemove" variant="outline-danger" onClick={(event: any) => this.removeItem(this.props.obj._id, event)}>⨯</Button>
                </div>
            </tr>
        );
    }
}

export default DataTable;