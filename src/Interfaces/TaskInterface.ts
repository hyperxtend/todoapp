 // Task interface
export interface TaskInterface {
  id: string;
  text: string;
  isCompleted: boolean;
}
// Task form interface
export interface TaskFormInterface {
  tasks: TaskInterface[];
  handleTaskCreate: (Task: TaskInterface) => void;
}
// Task list interface
export interface TaskListInterface {
  handleTaskUpdate: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleTaskRemove: (id: string) => void;
  handleTaskComplete: (id: string) => void;
  handleTaskBlur: (event: React.ChangeEvent<HTMLInputElement>) => void;
  tasks: TaskInterface[]
}
// Task item interface
export interface TaskItemInterface {
  handleTaskUpdate: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleTaskRemove: (id: string) => void;
  handleTaskComplete: (id: string) => void;
  handleTaskBlur: (event: React.ChangeEvent<HTMLInputElement>) => void;
  task: TaskInterface;
}